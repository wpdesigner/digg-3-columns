<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="wrap" style="   ">

	</div>	

  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>	

	</div>	
	
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      	
        <?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container'       => '',
   					'container_class' => 'menu-{menu slug}-container',
   					'container_id'    => '',
				    'menu_class'      => 'menu',
				    'menu_id'         => '',
				    'echo'            => true,
				    'fallback_cb'     => 'false',
				    'before'          => '',
				    'after'           => '',
				    'link_before'     => '',
				    'link_after'      => '',
				    'items_wrap'      => '%3$s',
				    'depth'           => 0,
				    'walker'          => ''
				) );
			?>
			
      </ul>
    
    </div>
  </div>

</nav> 

	<div class="header-wrap">
		<div class="header">
			<div class="head">
			    
			    <?php the_custom_logo(); ?>
			    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
			    <p class="site-description"><?php bloginfo( 'description' ); ?></p>
			
			</div><br><br><br>

		</div> 	
		<div class="row search">

		    <ul class="nav navbar-nav navbar-right">
		        <li><div class="col-md-4 menu-grids mgd1">
				
					<?php if(is_active_sidebar('menuwidget')) : ?>
							<div class="menu-widget-area">
							<?php dynamic_sidebar('menuwidget'); ?>
							</div>
					<?php endif; ?>
				</div></li>
		    </ul>	

		</div>
	</div>      
</header><!-- #masthead --><div class="clearfix"></div>

<div id="content" class="site-content">