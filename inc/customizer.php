<?php
/**
 * Digg 3 Columns Theme Customizer
 *
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function digg_3_columns_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'digg_3_columns_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'digg_3_columns_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'digg_3_columns_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function digg_3_columns_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function digg_3_columns_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

function digg_3_columns_register_theme_customizer( $wp_customize ) {


		$wp_customize->add_section( 'digg_3_columns_new_section_post' , array(
	   		'title'      =>  __( 'Post Settings','digg-3-columns' ),
	   		'description'=> '',
	   		'priority'   => 94,
		) );

		$wp_customize->add_section( 'digg_3_columns_new_section_wrapper_color' , array(
   		'title'      => __( 'Wrapper Color','digg-3-columns' ),
   		'description'=> '',
   		'priority'   => 9,
		) );


		//checkbox sanitization function
        function theme_slug_sanitize_checkbox( $input ){
             
            if($input == true){
            	return true;
            }
            else{
            	return false;
            }
        }


		$wp_customize->add_setting(
	        'digg_3_columns_post_thumb',
	        array(
	            'default'     => false,
	            'sanitize_callback' => 'theme_slug_sanitize_checkbox'
	        )
	    );

	    $wp_customize->add_setting(
				'digg_3_columns_wrapper_color',
				array(
					'default'     => '#666',
					'sanitize_callback' => 'sanitize_hex_color'
				)
			);


	    $wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post_thumb',
				array(
					'label'    => __( 'Hide Featured Image from top of Post', 'digg-3-columns' ),
					'section'    => 'digg_3_columns_new_section_post',
					'settings'   => 'digg_3_columns_post_thumb',
					'type'		 => 'checkbox',
					'priority'	 => 7
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'wrapper_color',
				array(
					'label'      => __('Wrapper Color', 'digg-3-columns' ),
					'section'    => 'digg_3_columns_new_section_wrapper_color',
					'settings'   => 'digg_3_columns_wrapper_color',
					'priority'	 => 1
				)
			)
		);
}
add_action( 'customize_register', 'digg_3_columns_register_theme_customizer' );

//////////////////////////////////////////////////////////////////
// Customizer - Add CSS
//////////////////////////////////////////////////////////////////
function digg_3_columns_customizer_css() {
    ?>
    <style type="text/css">
    	.wrap { background:<?php echo esc_attr(get_theme_mod( 'digg_3_columns_wrapper_color' )); ?>; }
    </style>

    <?php
}
add_action( 'wp_head', 'digg_3_columns_customizer_css' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function digg_3_columns_customize_preview_js() {
	wp_enqueue_script( 'digg_3_columns-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'digg_3_columns_customize_preview_js' );

?>