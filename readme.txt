=== Digg 3 Columns ===

Contributors: wpdesigner

Tags: blog, three-columns, custom-background, threaded-comments, translation-ready

Requires at least: 4.7
Tested up to: 4.9.4
Stable tag: 2.0.2
License: GNU General Public License v2 or later
License URI: LICENSE

Digg 3 Columns WordPress Theme

== Description ==

Elegant three columns WordPress theme loaded with two sidebars to display content, built with responsive CSS, you will enjoy beautiful layout and design on all screen resolutions.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== License ==

Digg 3 Columns WordPress Theme, Copyright (C) 2018, WPDesigner
Digg 3 Columns is distributed under the terms of the GNU GPL

== Credits ==

1) Framework
1.1) Underscores
Digg 3 Columns is based on Underscores. All the files in the theme package are from Underscores, unless stated otherwise.
Resource URI: http://underscores.me/
Copyright: Automattic, automattic.com
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

1.2) Bootstrap v3.3.7
Resource URI: https://getbootstrap.com/
Copyright: Twitter Inc
License: MIT
License URI: http://opensource.org/licenses/MIT

2) Fonts
2.1) Font Awesome
Resource URI: https://fontawesome.com/
Copyright: Dave Gandy
License: Font: SIL OFL 1.1, CSS: MIT License
License URI: https://fontawesome.com/license

== Changelog ==

= 2.0.2 =
* Released: April 2, 2018
* Fix: Styles, language file, tags, license, credits links & prefixes. 
* Removed: Extra files, folders & hard-coded text from theme.

= 2.0.1 - Mar 2 2018 =
* Changed: Redeveloped site & made it responsive.

= 1.0 - jan 22 2007 =
* Initial release