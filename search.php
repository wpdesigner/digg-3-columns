<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 */

get_header(); ?>
	
	<?php if(has_header_image()) : ?>
	<div id="masthead">
		<img class="banner" src="<?php echo esc_url(get_header_image()); ?>" >			
	</div>
	<?php endif; ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main col-md-8">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'digg-3-columns' ), '<span>' . get_search_query() . '</span>' );
				?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			?>
				<p align="center"> <?php the_posts_pagination(); ?></p>
			<?php

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->

		<?php if(is_active_sidebar('sidebar2')) : ?>
			<div class="sbar col-md-4">
				<?php dynamic_sidebar('sidebar2'); ?>
			</div>
		<?php endif; ?>
		
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
