<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */

get_header(); ?>
	
	<?php if(has_header_image()) : ?>
	<div id="masthead">
		<img class="banner" src="<?php echo esc_url(get_header_image()); ?>" >			
	</div>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-md-8">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-single', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->

		<?php if(is_active_sidebar('sidebar2')) : ?>
			<div class="sbar col-md-4">
				<?php dynamic_sidebar('sidebar2'); ?>
			</div>
		<?php endif; ?>
		
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
