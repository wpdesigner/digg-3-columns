<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>
<article class="main" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
	<?php digg_3_columns_post_thumbnail('thumb'); ?>
	<div class="post">
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		
		<?php
		endif; ?>		
	</header><!-- .entry-header -->

	<?php  digg_3_columns_posted_on(); ?>

	<div class="entry-content">

		<?php if (is_search() || is_archive() || is_home() ) { ?>
				<P>
				<?php echo get_the_excerpt(''); ?>

				</p>
			<?php } else {

			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'digg-3-columns' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'digg-3-columns' ),
				'after'  => '</div>',
			) );	}
		?>
	</div><!-- .entry-content -->
	</div><!-- .post -->
	<footer class="entry-footer">
		<?php digg_3_columns_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->

